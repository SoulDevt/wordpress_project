<?php
// Test
// add_action('wp_footer','myplugin_toto');

// function myplugin_toto()
// {
//     echo "<i>Mon plugin TOTO est activé !</i>";
// }

add_action('admin_menu','myplugin_addAdminLink');
function myplugin_addAdminLink(){
    // https://wordpress.org/support/article/roles-and-capabilities/
    add_menu_page(
        'Plugin TOTO Page',
        'Plugin TOTO',
        'manage_options',
        'monplugin/includes/toto-acp.php',
        '',
        'dashicons-chart-pie'
    );
}

// short code - [toto]
add_shortcode('toto','myplugin_totoShortcode');

function myplugin_totoShortcode(){
    return "<b>Mon shortcode TOTO est là!</b>";
}

// short code - [titi]
add_shortcode('titi','myplugin_titiShortcode');

function myplugin_titiShortcode($atts)
{
    $a = shortcode_atts(array(
        'x'=> 'un truc',
        'y'=> 'un autre truc',
    ),$atts);
    return "<b>x est {$a['x']} et y est  {$a['y']}</b>";
}
//check if user wants to delete table



function afficheForm(){
    echo'<form action="" method="post">
        <p>Voulez-vous supprimer la table ?</p>
        <div>
        <input type="submit" id="oui" name="oui" value="oui" checked>
        </div>

        <div>
        <input type="submit" id="non" name="non" value="non">
        </div>
        </form>';
}

//widgets 
class Toto_Widget extends Wp_Widget{
    public function __construct() {
        parent::__construct(
            'toto_widget',
            __('Widget Toto', 'mypluginlg'),
            array(
                'description'=>__('Widget simple issu du plugin TOTO','mypluginlg')
            ),
        );
        add_action('wp_loaded', array($this, 'save_email'));
    }

    public function widget($args,$instance)
    {
       //front
       $title = apply_filters('widget_title',$instance['title']);
       echo $before_widget.$before_title.$title.$after_title.'<form action="" method="post">
       <label for="email_user">Votre email: </label>
       <input type="email" id="email_user" name="email_user">
       <input type="submit">
       </form>'.$after_widget;
       global $wpdb;
       $results = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}toto_table");
        foreach($results as $element){
            echo($element->email);
        }
    }

    public function form($instance)
    {
        //back
        $title = isset($instance['title']) ? $instance['title']: '';
        echo'<label for="'.$this->get_field_name('title').'">Titre: </label>
        <input type="text" id="'.$this->get_field_id('title').'" name="'.$this->get_field_name('title').'" value="'.$title.'">';
    }

    public function update($new_instance,$old_instance)
    {
        $instance = array();
        $instance['title']= (!empty($new_instance['title'])) ? strip_tags($new_instance['title']):'';
        return $instance;
    }

    function save_email(){
        if(isset($_POST['email_user']) && !empty($_POST['email_user'])){
            global $wpdb;
            $email = $_POST['email_user'];
            $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}toto_table WHERE email=$email");
            if(is_null($row)){
                $wpdb->insert("{$wpdb->prefix}toto_table", array('email'=>$email));
            }
        }
    }
}

add_action('widgets_init','myplugin_totoWidget');

function myplugin_totoWidget()
{
    register_widget('Toto_widget');
}